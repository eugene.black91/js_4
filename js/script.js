// 1. Функции помогают повторять одно и то же действие в разных частях программы.
// 2. Аргументы нужны для того, чтобы сделать функцию более универсальной.
// 3.Оператор return используется в функциях для возвращения данных после выполнения работы функции. Если функция должна обработать какие-то данные и потом вернуть их, то для возвращения данных необходим оператор return.

let num1 = +prompt("Enter first number");
let num2 = +prompt("Enter second number");
let operator = prompt("Enter math operator");

function calc(firstNumber, secondNumber, operation) {
  switch (operation) {
    case "-":
      return firstNumber - secondNumber;
    case "+":
      return firstNumber + secondNumber;
    case "*":
      return firstNumber * secondNumber;
    case "/":
      return firstNumber / secondNumber;
  }
}

console.log(calc(num1, num2, operator));
